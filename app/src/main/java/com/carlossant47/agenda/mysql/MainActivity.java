package com.carlossant47.agenda.mysql;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.carlossant47.agenda.mysql.database.Contacto;
import com.carlossant47.agenda.mysql.database.ContactoPHP;
import com.carlossant47.agenda.mysql.database.Device;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombre;
    private EditText txtTel1;
    private EditText txtTel2;
    private EditText txtDomicilio;
    private EditText txtNotas;
    private CheckBox cbFavorito;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnLista;
    private Button btnSalir;
    private Contacto saveContact = null;
    private ContactoPHP ws;
    private long ID;
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        btnGuardar.setOnClickListener(this::btnGuardarAction);
        btnLista.setOnClickListener(this::btnListarAction);
        this.btnLimpiar.setOnClickListener(v -> limpiar());
    }

    private void init()
    {
        txtNombre = findViewById(R.id.txtNombre);
        txtTel1 = findViewById(R.id.txtTel1);
        txtTel2 = findViewById(R.id.txtTel2);
        txtDomicilio = findViewById(R.id.txtDomicilio);
        txtNotas = findViewById(R.id.txtNotas);
        cbFavorito = findViewById(R.id.cbFavorito);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLista = findViewById(R.id.btnLista);
        btnSalir = findViewById(R.id.btnSalir);

        ws = new ContactoPHP(getApplicationContext());
    }

    private void btnListarAction(View view) {
        Intent intent = new Intent(this, ListActivity.class);
        startActivityForResult(intent, 0);
    }


    private void btnGuardarAction(View view) {
        if(!validar()) {
            Toast.makeText(getApplicationContext(), "No puedes dejar los campos vacios",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        if(!Device.isInternetConnection(getApplicationContext()))
        {
            Toast.makeText(getApplicationContext(), "Requiere conexion a internet",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Contacto contacto = new Contacto();
        contacto.setNombre(getText(txtNombre));
        contacto.setDomicilio(getText(txtDomicilio));
        contacto.setNotas(getText(txtNotas));
        contacto.setTelefono1(getText(txtTel1));
        contacto.setTelefono2(getText(txtTel2));
        contacto.setFavorito(cbFavorito.isChecked());
        contacto.setSecureId(Device.getSecureId(getApplicationContext()));
        if (isEdit)
        {
            contacto.setId(saveContact.getId());
            ws.updateContacto(contacto);
            Toast.makeText(getApplicationContext(), "Se actualizo correctamente", Toast.LENGTH_SHORT).show();
        }
        else{
            ws.insertContacto(contacto);
            Toast.makeText(getApplicationContext(), "Se agrego correctamente", Toast.LENGTH_SHORT).show();
        }
        limpiar();

    }

    private void limpiar(){
        txtDomicilio.setText("");
        txtNombre.setText("");
        txtTel1.setText("");
        txtTel2.setText("");
        txtNotas.setText("");
        cbFavorito.setChecked(false);
        isEdit = false;
        saveContact = null;
        ID = 0;
    }


    private boolean validar(){
        if(validarCampo(txtDomicilio) || validarCampo(txtNombre) ||
                validarCampo(txtNotas) || validarCampo(txtTel1) || validarCampo(txtTel2))
            return false;
        else
            return true;
    }

    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(Activity.RESULT_OK == resultCode)
        {
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            saveContact = contacto;
            ID = saveContact.getId();
            txtNombre.setText(saveContact.getNombre());
            txtNotas.setText(saveContact.getNombre());
            txtTel1.setText(saveContact.getTelefono1());
            txtTel2.setText(saveContact.getTelefono2());
            txtDomicilio.setText(saveContact.getDomicilio());
            cbFavorito.setChecked(saveContact.isFavorito());
            isEdit = true;

        }
        else
        {
            limpiar();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
