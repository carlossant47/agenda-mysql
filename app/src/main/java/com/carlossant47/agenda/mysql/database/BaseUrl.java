package com.carlossant47.agenda.mysql.database;

public abstract class BaseUrl {
    //
    // private static final String baseURl = "https://149.56.108.229/agenda/";
    private static final String baseURl = "http://149.56.108.229/agenda/";
    public static final String URL_REGISTRAR = baseURl + "wsRegistro.php";
    public static final String URL_ACUTALIZAR = baseURl + "wsActualizar.php";
    public static final String URL_CONSULTAR_TODOS = baseURl + "wsConsultarTodos.php";
    public static final String URL_ELIMINAR = baseURl + "wsEliminar.php";
}
