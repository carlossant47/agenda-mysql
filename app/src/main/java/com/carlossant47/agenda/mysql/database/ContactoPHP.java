package com.carlossant47.agenda.mysql.database;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;

public class ContactoPHP implements Response.Listener<JSONObject>,Response.ErrorListener {

    private RequestQueue request;
    private JsonObjectRequest objectRequest;
    private ArrayList<Contacto> contactos = new ArrayList<>();

    public ContactoPHP(Context context)
    {
        this.request = Volley.newRequestQueue(context);
    }

    public void insertContacto(Contacto contacto) {
        String url = "";
        Parameters params = new Parameters();
        params.addParam("nombre", contacto.getNombre());
        params.addParam("telefono1", contacto.getTelefono1());
        params.addParam("telefono2", contacto.getTelefono2());
        params.addParam("direccion", contacto.getDomicilio());
        params.addParam("notas", contacto.getNotas());
        params.addParam("favorite", contacto.isFavorito() ? 1: 0);
        params.addParam("idMovil", contacto.getSecureId());
        url = params.createUrl(BaseUrl.URL_REGISTRAR);
        this.objectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(objectRequest);
    }

    public void updateContacto(Contacto contacto) {
        String url = "";
        Parameters params = new Parameters();
        params.addParam("_ID", contacto.getId());
        params.addParam("nombre", contacto.getNombre());
        params.addParam("telefono1", contacto.getTelefono1());
        params.addParam("telefono2", contacto.getTelefono2());
        params.addParam("direccion", contacto.getDomicilio());
        params.addParam("notas", contacto.getNotas());
        params.addParam("favorite", contacto.isFavorito() ? 1: 0);
        params.addParam("idMovil", contacto.getSecureId());
        url = params.createUrl(BaseUrl.URL_ACUTALIZAR);
        this.objectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(objectRequest);
    }

    public void deleteContacto(int id)
    {
        String url = "";
        Parameters parameters = new Parameters();
        parameters.addParam("_ID", id);
        url = parameters.createUrl(BaseUrl.URL_ELIMINAR);
        this.objectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(objectRequest);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("ERROR",error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("DATA", response.toString());
    }
}
