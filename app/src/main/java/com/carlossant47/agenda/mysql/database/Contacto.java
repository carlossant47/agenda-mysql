/*
 * Copyright (c) 2020. Carlos Santiago, no usar con fines comerciales, solo con fines educativos
 */

package com.carlossant47.agenda.mysql.database;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Contacto implements Serializable {

    private int id;
    private String nombre;
    private String telefono1;
    private String telefono2;
    private String domicilio;
    private String notas;
    private boolean favorito;
    private String secureId;
    private int status;

    public Contacto()
    {

    }

    public Contacto(JSONObject object)
    {
        try {
            this.setId(object.getInt("_ID"));
            this.setNombre(object.getString("nombre"));
            this.setTelefono1(object.getString("telefono1"));
            this.setTelefono2(object.getString("telefono2"));
            this.setDomicilio(object.getString("direccion"));
            this.setNotas(object.getString("notas"));
            this.setFavorito(object.getInt("favorite") == 1);
            this.setSecureId(object.getString("idMovil"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public Contacto(int id, String nombre, String telefono1, String telefono2, String domicilio, String notas, boolean favorito, String secureId, int status) {
        this.id = id;
        this.nombre = nombre;
        this.telefono1 = telefono1;
        this.telefono2 = telefono2;
        this.domicilio = domicilio;
        this.notas = notas;
        this.favorito = favorito;
        this.secureId = secureId;
        //this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public boolean isFavorito() {
        return favorito;
    }

    public void setFavorito(boolean favorito) {
        this.favorito = favorito;
    }

    public String getSecureId() {
        return secureId;
    }

    public void setSecureId(String secureId) {
        this.secureId = secureId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
