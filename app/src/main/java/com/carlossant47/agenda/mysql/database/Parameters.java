/*
 * Copyright (c) 2020. Carlos Santiago, no usar con fines comerciales, solo con fines educativos
 */
package com.carlossant47.agenda.mysql.database;
import android.util.Log;

import java.util.ArrayList;

public class Parameters {
    private ArrayList<Param> params = new ArrayList<>();

    public void addParam(String name, String value)
    {
        params.add(new Param(name, value));
    }
    public void addParam(String name, int value)
    {
        params.add(new Param(name, String.valueOf(value)));
    }
    public void addParam(String name, float value)
    {
        params.add(new Param(name, String.valueOf(value)));
    }

    public String createUrl(String url)
    {
        String urlParams = "";
        String format = "%s=%s&";
        for (int x = 0; x < this.params.size(); x++)
        {

            if((this.params.size() - 1) == x)
                format = "%s=%s";
            urlParams += String.format(format, params.get(x).getName(), params.get(x).getValue());
        }

        String finalUrl = String.format("%s?%s", url, urlParams);
        finalUrl = finalUrl.replace(" ", "%20");
        Log.e("URL", finalUrl);
        return finalUrl;
    }

    private class Param
    {
        private String name;
        private String value;
        public Param(String name, String value)
        {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
