package com.carlossant47.agenda.mysql;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.carlossant47.agenda.mysql.database.BaseUrl;
import com.carlossant47.agenda.mysql.database.Contacto;
import com.carlossant47.agenda.mysql.database.ContactoAdapter;
import com.carlossant47.agenda.mysql.database.ContactoPHP;
import com.carlossant47.agenda.mysql.database.Device;
import com.carlossant47.agenda.mysql.database.Parameters;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity implements
        Response.Listener<JSONObject>,Response.ErrorListener {
    private BottomSheetBehavior mBehavior;
    private BottomSheetDialog mBottomSheetDialog;
    private Button btnNuevo;
    private ListView listContactos;
    private ContactoPHP ws;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Contacto> list;
    private ContactoAdapter adapter;
    private View bottom_sheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        init();
    }

    private void init()
    {
        listContactos = findViewById(R.id.listContactos);
        request = Volley.newRequestQueue(getApplicationContext());

        ws = new ContactoPHP(getApplicationContext());
        list = new ArrayList<>();

        consultContactos();
        listContactos.setOnItemClickListener((parent, view, position, id) -> {

        });



    }

    private void consultContactos()
    {
        Parameters params = new Parameters();
        params.addParam("idMovil", Device.getSecureId(getApplicationContext()));
        String url = params.createUrl(BaseUrl.URL_CONSULTAR_TODOS);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }
    private void deleteContacto(int id)
    {
        Parameters params = new Parameters();
        params.addParam("_ID", id);
        String url = params.createUrl(BaseUrl.URL_ELIMINAR);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("ERROR", error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.e("RESILT", response.toString());
        try {
            String type = response.getString("op");
            if(type.equals("consultar"))
            {
                load(response.optJSONArray("contactos"));
            }
            if(type.equals("eliminar"))
            {
                if(response.getBoolean("status"))
                {
                    Toast.makeText(this, "Se elimino correctamente", Toast.LENGTH_SHORT).show();
                    adapter.delete(response.getInt("id"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void load(JSONArray array) {
        try {
            for (int x = 0; x < array.length(); x++) {
                JSONObject object = array.getJSONObject(x);
                Contacto contacto = new Contacto(object);
                list.add(contacto);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adapter = new ContactoAdapter(this, R.id.lbNombre, R.layout.contacto_layout, list);
        listContactos.setAdapter(adapter);
        Log.e("DATA", array.toString());
    }

    private class ContactoAdapter extends ArrayAdapter<Contacto> {
        private Activity context;
        private int textViewResourceID;
        private ArrayList<Contacto> contactos;
        private LayoutInflater inflater;

        public ContactoAdapter(Activity context, @IdRes int idTextView, @LayoutRes int  layoutIdResource, ArrayList<Contacto> items)
        {
            super(context, idTextView, items);
            this.context = context;
            this.textViewResourceID = layoutIdResource;
            this.contactos = items;
            this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void delete(int id)
        {
            for (int x = 0; x < this.contactos.size(); x++)
                if(this.contactos.get(x).getId() == id)
                {
                    this.contactos.remove(x);
                    notifyDataSetChanged();
                    break;
                }
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = this.inflater.inflate(this.textViewResourceID, parent, false);
            TextView lbNombre  = view.findViewById(R.id.lbNombre);
            TextView lbTelefono  = view.findViewById(R.id.lbTelefono);
            ImageButton btFavorite = view.findViewById(R.id.btnFavorite);
            Button btnEditar = view.findViewById(R.id.btnEditar);
            Button btnEliminar = view.findViewById(R.id.btnEliminar);
            btFavorite.setImageResource(contactos.get(position).isFavorito() ?
                    R.drawable.ic_start_checked : R.drawable.ic_start);
            lbNombre.setText(contactos.get(position).getNombre());
            lbTelefono.setText(contactos.get(position).getTelefono1());
            btnEditar.setOnClickListener(this.btnEditarAction(position));
            btnEliminar.setOnClickListener(this.btnEliminarAction(contactos.get(position).getId()));
            return view;
        }

        private View.OnClickListener btnEditarAction(int position)
        {
            return v -> {
                Bundle oBundle = new Bundle();
                oBundle.putSerializable("contacto", contactos.get(position));
                Intent i = new Intent();
                i.putExtras(oBundle);
                setResult(Activity.RESULT_OK, i);
                finish();
            };
        }

        private View.OnClickListener btnEliminarAction(int id)
        {
            return v -> deleteContacto(id);
        }


        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            return getView(position, convertView, parent);
        }
    }


}
